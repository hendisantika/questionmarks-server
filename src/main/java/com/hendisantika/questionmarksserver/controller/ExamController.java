package com.hendisantika.questionmarksserver.controller;

import com.hendisantika.questionmarksserver.dto.ExamCreationDTO;
import com.hendisantika.questionmarksserver.dto.ExamUpdateDTO;
import com.hendisantika.questionmarksserver.model.Exam;
import com.hendisantika.questionmarksserver.service.ExamRepository;
import com.hendisantika.questionmarksserver.util.DTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : questionmarks-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/12/17
 * Time: 11.13
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/exams")
public class ExamController {
    private ExamRepository examRepository;

    public ExamController(ExamRepository examRepository) {
        this.examRepository = examRepository;
    }

    @GetMapping
    public List<Exam> getExams() {
        return examRepository.findAll();
    }

    @PostMapping
    public void newExam(@DTO(ExamCreationDTO.class) Exam exam) {
        examRepository.save(exam);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void editExam(@DTO(ExamUpdateDTO.class) Exam exam) {
        examRepository.save(exam);
    }
}
