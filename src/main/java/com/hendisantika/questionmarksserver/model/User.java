package com.hendisantika.questionmarksserver.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : questionmarks-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/12/17
 * Time: 08.11
 * To change this template use File | Settings | File Templates.
 */

@Data
@Entity
public class User {
    @Id
    private String id;
}
