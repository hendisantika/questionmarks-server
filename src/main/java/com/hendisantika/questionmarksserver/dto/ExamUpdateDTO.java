package com.hendisantika.questionmarksserver.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by IntelliJ IDEA.
 * Project : questionmarks-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/12/17
 * Time: 11.04
 * To change this template use File | Settings | File Templates.
 */

@Getter
@Setter
public class ExamUpdateDTO {
    @JsonIgnore
    private final LocalDateTime editedAt = LocalDateTime.now();

    @Id
    @NotNull
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String description;
}
