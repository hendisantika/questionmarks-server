package com.hendisantika.questionmarksserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionmarksServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuestionmarksServerApplication.class, args);
    }
}
