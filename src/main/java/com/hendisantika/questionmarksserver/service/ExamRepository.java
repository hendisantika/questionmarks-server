package com.hendisantika.questionmarksserver.service;

import com.hendisantika.questionmarksserver.model.Exam;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : questionmarks-server
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 26/12/17
 * Time: 11.06
 * To change this template use File | Settings | File Templates.
 */
public interface ExamRepository extends JpaRepository<Exam, Long> {
}
